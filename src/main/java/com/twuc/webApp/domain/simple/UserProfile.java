package com.twuc.webApp.domain.simple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 64, nullable = false)
    private String name;
    private Short yearOfBirth;

    public UserProfile() {
    }

    private UserProfile(String name, Short yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Short getYearOfBirth() {
        return yearOfBirth;
    }
}
